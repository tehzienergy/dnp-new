ymaps.ready(init);
function init(){
  var myGeocoder = ymaps.geocode("Большая Сухаревская площадь, д. 3, Москва, Россия");
  myGeocoder.then(
      function (res) {
        var myMap = new ymaps.Map("map", {
            center: res.geoObjects.get(0).geometry.getCoordinates(),
            controls: [],
            zoom: 13
        });
        var myPlacemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {}, {
          iconLayout: 'default#image',
          iconImageHref: 'img/marker.svg',
          iconImageSize: [45, 66],
        });
        myMap.geoObjects.add(myPlacemark);
      },
  );   
}