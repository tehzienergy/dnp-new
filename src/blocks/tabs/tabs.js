if ($(window).width() > 1199) {
  $('.tabs').sticky({
    topSpacing: 120
  });
}

$('.tabs__item').click(function () {
  $('.tabs__item').removeClass('tabs__item--active');
  $(this).addClass('tabs__item--active');
  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top - 120
  }, 1000);
});



if ($(window).width() > 1199 && $('.tabs').length > 0) {
  $(window).scroll(function () {
    var scrollPosition = $(window).scrollTop();
    var tabsPosition = $('#sticky-wrapper').offset().top + 60;
    if (scrollPosition >= tabsPosition) {
      $('.tabs__wrapper').addClass('tabs__wrapper--fixed');
    }
    else {
      $('.tabs__wrapper').removeClass('tabs__wrapper--fixed');
    }
  });
}



if ($('.tabs').length > 0) {

  function magicLine(initialPosition) {
    var underlineOffset = 15;
    if ($(window).width() < 1199) {
      underlineOffset = 10;
    }
    $('.tabs__underline').width($('.tabs__item--active').width());
    $('.tabs__underline').css('left', $('.tabs__item--active').position().left + underlineOffset);
  };

  magicLine();

  $(window).scroll(function () {
    var scrollPosition = $(window).scrollTop();
    $('.section').each(function (i) {
      if ($(this).position().top <= scrollPosition - 120) {
        $('.tabs__item').removeClass('tabs__item--active');
        $('.tabs__item').eq(i).addClass('tabs__item--active');
        magicLine();
      }
    });
  });
}