$('.select__input').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form select--common',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--branch').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form select--modal',
    width: '100%',
    placeholder: 'Выберите направление',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--filter').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form select--filter',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});

$('.select__input--filter-multiple').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form select--filter',
    width: '100%',
    dropdownAutoWidth: true,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select'),
    templateResult: function(option) {
      var $option = $(
        '<div class="select2-option--checkbox">' + option.text + '</div>'
      );
      return $option;
    }
  });
});


$('.select__input--field').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form',
    placeholder: 'Специализация',
    width: '100%',
    closeOnSelect: false,
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select'),
    templateResult: function(option) {
      var $option = $(
        '<div class="select2-option--checkbox">' + option.text + '</div>'
      );
      return $option;
    }
  });
});


$('.select__input--country').each(function() {
  var select = $(this);
  select.select2({
    minimumResultsForSearch: -1,
    containerCssClass: 'select--form',
    placeholder: 'Выбор страны',
    width: '100%',
    dropdownCssClass: 'select__dropdown--common',
    dropdownParent: select.closest('.select')
  });
});


$('.select__input').on("select2:open", function () {
    const ps = new PerfectScrollbar('.select2-results__options');
    setTimeout(function () {
      ps.update()
    }, 1);
});