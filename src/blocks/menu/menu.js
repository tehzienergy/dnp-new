$('.menu__btn').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('menu__btn--active');
  $('.menu__wrapper').toggleClass('menu__wrapper--active').css('padding-top', headerHeight);
  $('.header__bottom').toggleClass('header__bottom--white');
  $('.breadcrumbs').toggleClass('breadcrumbs--hidden');
  $('body').toggleClass('body--fixed');
});