var time = 3;
var $bar,
  $slick,
  isPause,
  tick,
  percentTime;

$slick = $('.front__content');
$slick.slick({
  arrows: false,
  dots: true,
  autoplay: true,
});

$bar = $('.front__progress-content');

function startProgressbar() {
  resetProgressbar();
  percentTime = 0;
  isPause = false;
  tick = setInterval(interval, 10);
}

function interval() {
  if (isPause === false) {
    percentTime += 1 / (time + 0.1);
    $bar.css({
      width: percentTime + "%"
    });
    if (percentTime >= 100) {
      $slick.slick('slickNext');
      startProgressbar();
    }
  }
}


function resetProgressbar() {
  $bar.css({
    width: 0 + '%'
  });
  clearTimeout(tick);
}

startProgressbar();