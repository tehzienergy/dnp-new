var headerHeight = $('.header').outerHeight();

$(window).on('load resize', function() {
  $(".wrapper").css('padding-top', headerHeight);
});

$('.header__settings-trigger').click(function(e) {
  e.preventDefault();
  $('.header__bottom-wrapper').toggleClass('header__bottom-wrapper--hidden');
  $('.header__settings-content').toggleClass('header__settings-content--active');
});